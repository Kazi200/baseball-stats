
# Summary
<br>
In this analysis we'll be looking at a dataset of 1157 baseball players and some of their traits and statistics. Namely: Height, Weight, Home runs, Batting average, and Handedness (are you right handed, left handed, or ambidextrous). 
<br><br>
The primary focus is to ascertain whether there is a certain trait, or set of traits, that lead to a better batting average or more home runs. 
<br><br>

# Design
<br>
The charts were designed to be as readable as possible so that the message comes across very easily. 
<br><br>

First version available to view here:<br>
<https://public.tableau.com/profile/mohomed.kazi#!/vizhome/Project4-v1/Story1>
<br><br>
Final version available to view here:<br>
<https://public.tableau.com/profile/mohomed.kazi#!/vizhome/Project4-Final/Story1>
<br><br>

Design decisions:
<br>
1. All charts were made with the same colour palette, which was defined by the "Handedness" category. All approppriate axes were labelled with the handedness category to negate the need for a legend. 

2. A consistent filter was applied to the data, removing the players with no batting average.

3. A box plot was added to verify the conclusions drawn from the bar graphs. Additionally, box plots give us more information than bar graphs, which (in this case particularly) help us to draw more accurate conclusions about our data. 

4. The graphs pertaining to height and weight were removed, since there were no strongs conclusions to be drawn from them.

5. Names of fields and values were changed to be more readable. For example, HR changed to Home runs, AVG changed to Batting average, etc..

6. Pie charts were changed to bar graphs for improved legibility. Pie charts have also come under scrutiny for being misleading. 

7. A scatter plot was used to show correlation between the Home runs and Batting average variables. Trend lines were added in to show the positive correlation mnore effectively. 

8. Bar graphs feature prominently in this analysis since they are easy to read, and show results quite effectively. Where necessary, text labels were used for extra clarity (for example, in cases where the bar graphs are particularly close together). 
<br><br>

Regarding the actual findings from the visualisations, we were anble to ascertain the following:
1. There is a positive correlation between home runs and a players batting average, and this is independent of their handedness category.
<br>
2. Overall, it would seem that left handed players score more home runs on average, but the bar chart that shows this is slightly misleading. 
<br>
3. Using a box plot shows us that we were incorrect in our previous assertion. It turns out that right handed players have a higher number of home runs on average. The previous assertion was misleading in that it did not take into account the outliers in the data.

<br>

# Feedback
<br>
Feedback was taken from my colleague, who viewed the first version of the story, and advised as follows:
<br><br>

1. Keep a consistent colour palette through the story.
2. A box plot could show further insight into the data.
3. The BMI calculation and related graphs are unnecessary, since we do not see any correlation between those variables and Home runs, and Batting average.
<br><br>

# Resources
<br>
No outside resources were used for this analysis.
